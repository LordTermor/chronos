#pragma once

#include <QDialog>
#include <ProcessTimingModel.h>

namespace Ui {
class JournalViewer;
}

class JournalViewer : public QDialog
{
    Q_OBJECT

public:
    explicit JournalViewer(QWidget *parent = nullptr);
    ~JournalViewer();

    QString getFileName() const;
    bool load(const QString &value);

private slots:

    void on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime);

    void on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime);

    void on_lineEdit_textEdited(const QString &arg1);

    void filter();
private:
    QList<ProcessTiming> list;
    ProcessTimingModel model;
    QString fileName;
    Ui::JournalViewer *ui;
};
