#include "EvaluationService.h"
#include <QtConcurrent/QtConcurrent>
#include <SimpleCrypt.h>
#include <QSettings>
static SimpleCrypt crypto(Q_UINT64_C(0x88285487487384));
EvaluationService::EvaluationService(QObject *parent) : QObject(parent)
{

}

void EvaluationService::setupEvaluation()
{
    QSettings("cache",QSettings::IniFormat).setValue("evaluation",crypto.encryptToString(QDateTime::currentDateTime().toString(Qt::DateFormat::ISODate)));
}

bool EvaluationService::checkEvaluation()
{
    auto date = decypherDate();

    if(!date.isValid()){
        emit evaluationPeriodIsOver();
        return false;
    }

   if (date.daysTo(QDateTime::currentDateTime())>30){
       emit evaluationPeriodIsOver();
       return false;
   }
   return true;
}

uint8_t EvaluationService::daysLeft()
{
    return 30-decypherDate().daysTo(QDateTime::currentDateTime());
}

QDateTime EvaluationService::decypherDate()
{
    auto dateString = QSettings("cache",QSettings::IniFormat).value("evaluation", "undefined").toString();
    if (dateString=="undefined"){
        return QDateTime();
}
    dateString = crypto.decryptToString(dateString);

    return QDateTime::fromString(dateString,Qt::DateFormat::ISODate);
}
