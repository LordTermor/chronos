#pragma once
#include <QAbstractTableModel>
#include <ProcessTiming.h>

class ProcessTimingModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ProcessTimingModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;


    QList<ProcessTiming> list() const;
    void setList(const QList<ProcessTiming> &list);

    void replaceItem(int index, ProcessTiming& timing);
    void addItem(const ProcessTiming& timing);



    static void saveJournal(QList<ProcessTiming> timings,QString journalFileName);
    static QList<ProcessTiming> loadJournal(QString journalFileName);



    bool showTime() const;
    void setShowTime(bool showTime);

signals:



private:
    bool m_showTime = true;
    QList<ProcessTiming> m_list;

};
