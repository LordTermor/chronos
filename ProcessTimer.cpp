#include "ProcessTimer.h"
#include <QTimerEvent>
ProcessTimer::ProcessTimer(QObject* parent): QObject(parent)
{

}

void ProcessTimer::connectToModel(ProcessTimingModel &model)
{
    connect(this,&ProcessTimer::measurementsUpdate,[&model](UpdateType type,ProcessTiming process){
        switch (type) {
        case NewMeasurement:{
            model.addItem(process);
            break;
        }
        case AdditionalMeasurement:{
            model.replaceItem(model.list().length()-1,process);
            break;
        }
        case StopMeasurement:{
            break;
        }
        }
    });
}

void ProcessTimer::startMeasurement(const QString &processName)
{
    if(started){
        stopMeasurement();
    }
    startTime = QDateTime::currentDateTime();
    started = true;

    currentProcessFileName = processName;


    emit measurementsUpdate(UpdateType::NewMeasurement,{currentProcessFileName,startTime,startTime});
    measurementTimerId = this->startTimer(1);


}

void ProcessTimer::stopMeasurement()
{
    if(!started){
        return;
    }
    killTimer(measurementTimerId);
    started = false;



    emit measurementsUpdate(UpdateType::StopMeasurement,{currentProcessFileName,startTime, QDateTime::currentDateTime()});

    currentProcessFileName = QString();
}

void ProcessTimer::timerEvent(QTimerEvent *event)
{
    if(event->timerId()==measurementTimerId){
        emit measurementsUpdate(UpdateType::AdditionalMeasurement,
        {currentProcessFileName,startTime,QDateTime::currentDateTime()});
    }
}
