#include "PromptDialog.h"
#include "ui_PasswordPromptDialog.h"
#include <QCryptographicHash>

PromptDialog::PromptDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PasswordPromptDialog)
{
    ui->setupUi(this);

    connect(ui->lineEdit,&QLineEdit::textChanged,[this](const QString& password){
        this->password = password;
    });
}

PromptDialog::~PromptDialog()
{
    delete ui;
}

bool PromptDialog::getIsAuthorized(const QByteArray &hash)
{
    PromptDialog dialog;

    return  (dialog.exec() == QDialog::Accepted) && hash==QCryptographicHash::hash(dialog.password.toLatin1(),QCryptographicHash::Sha256);
}

bool PromptDialog::activate()
{
    PromptDialog dialog;

    dialog.ui->label->setText("Введите лицензионный ключ продукта");
    dialog.ui->label_2->setText("Ключ");
    dialog.ui->lineEdit->setEchoMode(QLineEdit::Normal);

    dialog.setWindowTitle("Активация");

    return (dialog.exec() == QDialog::Accepted && dialog.password.toLatin1()=="c280d04d-6bae-4773-afb6-eba0acf87cd1");
}
