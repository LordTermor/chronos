#pragma once
#include <QDialog>
#include <QSettings>

namespace Ui {
class WhitelistDialog;
}

class WhitelistDialog : public QDialog
{
    Q_OBJECT

public:
    explicit WhitelistDialog(QWidget *parent = nullptr);
    ~WhitelistDialog();

    QList<QString> getWhiteListFileNames() const;
    void setWhiteListFileNames(const QList<QString> &value);

private:
    Ui::WhitelistDialog *ui;
    QList<QString> whiteListFileNames;
protected:
    void closeEvent(QCloseEvent *) override;
public slots:
    void open(const QString& fileName = "WhiteList.txt");
    void accept();
private slots:
    void reset();
    void on_unlockButton_clicked();
    void on_saveButton_clicked();
};
