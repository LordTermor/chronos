#pragma once
#include <QObject>
#include <QList>
#include <ProcessTimingModel.h>
#include <ProcessTiming.h>
enum UpdateType{
    NewMeasurement,
    AdditionalMeasurement,
    StopMeasurement
};

class ProcessTimer : public QObject
{
    Q_OBJECT
public:
    ProcessTimer(QObject* parent = nullptr);
    virtual ~ProcessTimer() {}

    void connectToModel(ProcessTimingModel& model);
public slots:
    void startMeasurement(const QString& processName);
    void stopMeasurement();

signals:
    void measurementsUpdate(UpdateType type, ProcessTiming timing);

protected:
    void timerEvent(QTimerEvent *event) override;
private:
    bool started;
    QString currentProcessFileName;
    QDateTime startTime;
    int measurementTimerId;

};
