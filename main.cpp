#include <QApplication>
#include "MainWindow.h"

int main(int argc,char** argv)
{

    QApplication app(argc,argv);

    bool start = true;
    bool lastReturnCode = 0;
    bool hidden = (argc>1 && QString::fromLatin1(argv[1])=="--hidden");
    if(hidden){
        MainWindow w;

        w.start();
        lastReturnCode = QApplication::exec();

        start = w.m_needsRestart;
    }
    while(start){

        MainWindow w;

        w.show();
        lastReturnCode = QApplication::exec();

        start = w.m_needsRestart;
    }
    return lastReturnCode;
}
