#include "MainWindow.h"
#include "ui_MainWindow.h"
#include <QMessageBox>
#include <ProcessTimingModel.h>
#include <QFileDialog>
#include <QTextStream>
#include <PromptDialog.h>
#include <QtDebug>
#include <QCryptographicHash>
#include <QCloseEvent>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{


    if(QSettings("cache",QSettings::IniFormat).value("firstRun",true).toBool()){
        evaluationService.setupEvaluation();
    }
    ui->setupUi(this);

    bool whiteList = QSettings("cache",QSettings::IniFormat).value("whitelist",false).toBool();
    if(!whiteList){
        ui->menu_3->menuAction()->setVisible(false);
        ui->menu_3->setVisible(false);
    }

    ui->tableView->setModel(&model);
    timer.connectToModel(model);
    model.setShowTime(true);

    connect(&watcher,&WindowWatcher::processChanged,this,[=](QString processFileName){
        timer.stopMeasurement();
        if(whiteList){
        for(auto el : whiteListDialog.getWhiteListFileNames()){
            if (!el.isEmpty() && processFileName.toLower().endsWith(el.toLower())){
                timer.startMeasurement(processFileName);
                return;
            }
        }}
        else{
            if(!processFileName.contains("Chronos.exe"))
                timer.startMeasurement(processFileName);
        }
    },Qt::QueuedConnection);



    if(QSettings("cache",QSettings::IniFormat).value("activated").toByteArray()
            ==QCryptographicHash::hash("c280d04d-6bae-4773-afb6-eba0acf87cd1",QCryptographicHash::Sha256).toHex()){
        ui->action_activate->setVisible(false);
    } else{

        if(evaluationService.daysLeft()<=0){
            on_action_activate_triggered();
            if(QSettings("cache",QSettings::IniFormat).value("activated").toByteArray()
                    !=QCryptographicHash::hash("c280d04d-6bae-4773-afb6-eba0acf87cd1",QCryptographicHash::Sha256).toHex()){
                qApp->exit();
            }
        }
        ui->action_activate->setText("Активация ("+QString::number(evaluationService.daysLeft())+" дн.)");
    }
    auto showAction = new QAction("Показать");

    connect(showAction,&QAction::triggered,[this](){
        this->setVisible(true);
    });

    auto quitAction = new QAction("Выйти");
    connect(quitAction,&QAction::triggered,[](){
        qApp->quit();
    });

    auto menu = new QMenu(this);

    menu->addActions({showAction,quitAction});
    icon.setIcon(QIcon(":/timer"));
    icon.setContextMenu(menu);
    icon.setVisible(true);
    icon.setToolTip("Замер времени не проводится");
}

MainWindow::~MainWindow()
{
    watcher.stopPolling();
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{
    auto pwd = QSettings("cache",QSettings::IniFormat).value("pwd",
                                                             QCryptographicHash::hash(QString("teftelya").toLatin1(),QCryptographicHash::Sha256).toHex());
    if(PromptDialog::getIsAuthorized(QByteArray::fromHex(pwd.toByteArray()))){
        if(!m_started){
            watcher.startPolling();
            setStarted(true);
        } else{
            watcher.stopPolling();
            setStarted(false);
        }
    }

}

void MainWindow::on_action_about_triggered()
{
    QMessageBox::about(this,"О программе","Хронос - следи за своим временем");
}

void MainWindow::on_action_close_triggered()
{
    qApp->exit();
}

void MainWindow::on_action_journal_open_triggered()
{
    auto string = QFileDialog::getOpenFileName(this,"Выберите файл журнала");
    if (string.isEmpty()){return;}

    if(!journalDialog.load(string)){
        QMessageBox::critical(this,"Ошибка","Невозможно открыть файл журнала!");
        return;
    }

    journalDialog.open();
}

void MainWindow::setStarted(bool started)
{
    m_started = started;
    if(started){
        icon.setToolTip("Идет замер времени");
        ui->pushButton->setText("Остановить");
        ui->label->setText("Идет замер времени");
    } else{
        icon.setToolTip("Замер времени не проводится");
        ui->pushButton->setText("Начать");
        ui->label->setText("Замер времени не проводится");
    }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if(icon.isVisible()){
        this->setVisible(false);
        event->ignore();
        return;
    }
    event->accept();
}

void MainWindow::on_action_whitelist_settings_triggered()
{
    whiteListDialog.open();
}

void MainWindow::on_action_preferences_triggered()
{
    preferencesDialog.exec();
    if(preferencesDialog.needsRestart){
        this->m_needsRestart = true;
        qApp->quit();
    }
}

void MainWindow::on_pushButton_2_clicked()
{
    auto string = QFileDialog::getSaveFileName(this,"Выберите, куда сохранить файл журнала");
    if(string.isEmpty()) return;
    ProcessTimingModel::saveJournal(model.list(),string);
}

void MainWindow::on_action_whitelist_export_triggered()
{
    auto string = QFileDialog::getSaveFileName(this,"Выберите файл для экспорта");

    QFile file(string);
    if (file.open(QFile::WriteOnly| QFile::Truncate | QFile::Text)){

        QTextStream stream(&file);
        for(auto& listElement: whiteListDialog.getWhiteListFileNames()){
            if(listElement!="\n")
                stream << listElement << "\n";
        }

        file.close();
        QMessageBox::information(this,"Результат","Экспортировано");
    }

}

void MainWindow::on_action_whitelist_import_triggered()
{
    auto string = QFileDialog::getOpenFileName(this,"Выберите файл для импорта");
    if(string.isEmpty()){return;}
    whiteListDialog.open(string);

}

void MainWindow::on_action_journal_export_triggered()
{
    on_pushButton_2_clicked();
}

void MainWindow::on_action_activate_triggered()
{
    if(PromptDialog::activate()){
        QSettings("cache",QSettings::IniFormat).setValue("activated",QCryptographicHash::hash("c280d04d-6bae-4773-afb6-eba0acf87cd1",QCryptographicHash::Sha256).toHex());
        ui->action_activate->setVisible(false);
    }
}
