#include "WhitelistDialog.h"
#include "ui_WhitelistDialog.h"
#include <PromptDialog.h>
#include <QCryptographicHash>
#include <QTextStream>

WhitelistDialog::WhitelistDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WhitelistDialog)
{
    ui->setupUi(this);

    QFile file("whiteList.txt");
    if (file.open(QFile::ReadOnly| QFile::Text)){

        QTextStream stream(&file);
        while(!stream.atEnd()){
            QString string = stream.readLine();
            if(!string.isEmpty())
                whiteListFileNames << string;
        }
        file.close();
    }
    for(auto string : whiteListFileNames){
        ui->plainTextEdit->appendPlainText(string);
    }
}

WhitelistDialog::~WhitelistDialog()
{
    delete ui;
}

QList<QString> WhitelistDialog::getWhiteListFileNames() const
{
    return whiteListFileNames;
}

void WhitelistDialog::setWhiteListFileNames(const QList<QString> &value)
{
    whiteListFileNames = value;
}

void WhitelistDialog::closeEvent(QCloseEvent *)
{
   reset();
}

void WhitelistDialog::open(const QString& fileName)
{
    ui->plainTextEdit->clear();
    whiteListFileNames.clear();
    QFile file(fileName);
    if (file.open(QFile::ReadOnly| QFile::Text)){

        QTextStream stream(&file);
        while(!stream.atEnd()){
            QString string = stream.readLine();
            if(!string.isEmpty())
                whiteListFileNames << string;
        }
        file.close();
    }
    for(auto string : whiteListFileNames){
        ui->plainTextEdit->appendPlainText(string);
    }
    QDialog::open();
}

void WhitelistDialog::accept()
{
    auto result = ui->plainTextEdit->toPlainText();

    whiteListFileNames = result.split("\n");

    QFile file("WhiteList.txt");
    if (file.open(QFile::WriteOnly| QFile::Truncate | QFile::Text)){

        QTextStream stream(&file);
        for(auto& listElement: whiteListFileNames){
            if(listElement!="\n")
                stream << listElement << "\n";
        }

        file.close();
    }
    QDialog::accept();
}

void WhitelistDialog::reset()
{
    ui->saveButton->setEnabled(false);
    ui->plainTextEdit->setEnabled(false);
    ui->unlockButton->setVisible(true);
    ui->label->setText("Для внесения изменений необходимы\nправа администратора");
}

void WhitelistDialog::on_unlockButton_clicked()
{
    auto pwd = QSettings("cache",QSettings::IniFormat).value("pwd",
                                                             QCryptographicHash::hash(QString("teftelya").toLatin1(),QCryptographicHash::Sha256).toHex());
    if(PromptDialog::getIsAuthorized(QByteArray::fromHex(pwd.toByteArray()))){
        ui->saveButton->setEnabled(true);
        ui->plainTextEdit->setEnabled(true);
        ui->unlockButton->setVisible(false);
        ui->label->setText("Введите процессы для замера.\nКаждый - с новой строки");
    }
}

void WhitelistDialog::on_saveButton_clicked()
{
    accept();
    reset();
}
