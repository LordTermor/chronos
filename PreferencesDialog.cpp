#include "PreferencesDialog.h"
#include "ui_PreferencesDialog.h"
#include <QListWidgetItem>
#include <PromptDialog.h>
#include <QCryptographicHash>
#include <QDir>
PreferencesDialog::PreferencesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PreferencesDialog)
{
    ui->setupUi(this);
    ui->checkBox_2->setChecked(QSettings("cache",QSettings::IniFormat).value("whitelist").toBool());
    connect(this,&QDialog::accepted,[this](){
        closeEvent(nullptr);
    });
    ui->checkBox->setChecked(autorun.value("Chronos",QVariant::Invalid).isValid());
}

PreferencesDialog::~PreferencesDialog()
{
    delete ui;
}

void PreferencesDialog::on_pushButton_2_clicked()
{
    auto pwd = QSettings("cache",QSettings::IniFormat).value("pwd",
                                                             QCryptographicHash::hash(QString("teftelya").toLatin1(),QCryptographicHash::Sha256).toHex());

    if(PromptDialog::getIsAuthorized((QByteArray::fromHex(pwd.toByteArray())))){
        ui->horizontalWidget_2->setEnabled(true);
        ui->checkBox->setEnabled(true);
        ui->checkBox_2->setEnabled(true);
        ui->label->setText("");
        ui->pushButton_2->setEnabled(false);
    }
}

void PreferencesDialog::closeEvent(QCloseEvent *)
{
    ui->horizontalWidget_2->setEnabled(false);
    ui->checkBox->setEnabled(false);
    ui->checkBox_2->setEnabled(false);
    ui->label->setText("Для внесения изменений необходимы\nправа администратора");
    ui->pushButton_2->setEnabled(true);
}

void PreferencesDialog::on_pushButton_clicked()
{
    if(ui->lineEdit->text().isEmpty()){
        return;
    }
    auto password = ui->lineEdit->text();
    auto hash = QCryptographicHash::hash(password.toLatin1(),QCryptographicHash::Sha256).toHex();
    QSettings("cache",QSettings::IniFormat).setValue("pwd",hash);
    ui->lineEdit->clear();

}

void PreferencesDialog::on_checkBox_2_stateChanged(int arg1)
{
        QSettings("cache",QSettings::IniFormat).setValue("whitelist",arg1==Qt::Checked);
    needsRestart = true;
}

void PreferencesDialog::on_checkBox_stateChanged(int arg1)
{
    if(arg1==Qt::Checked){
        autorun.setValue("Chronos", QDir::toNativeSeparators(QCoreApplication::applicationFilePath())+" --hidden");
        autorun.sync();
    } else{
        autorun.remove("Chronos");
    }
}
