#include "WindowWatcher.h"
#include <QtDebug>
#include <winuser.h>
#include <QtConcurrent/QtConcurrent>
#include <string>
WindowWatcher::WindowWatcher(QObject *parent): QObject(parent)
{

    QtConcurrent::run([this](){
        forever{
            if(exit){
                return;
            }
            if(started){
                auto window = GetForegroundWindow();
                DWORD processId = 0;
                GetWindowThreadProcessId(window,&processId);

                HANDLE Handle = OpenProcess(
                                  PROCESS_QUERY_INFORMATION | PROCESS_VM_READ,
                                  FALSE,
                                  processId
                                );


                wchar_t string[MAX_PATH];
                ulong max = MAX_PATH;
                QueryFullProcessImageNameW(Handle,0,string,&max);

                auto processPath = QString::fromWCharArray(string,max);
                if(processPath!=lastProcessPath){
                    lastProcessPath=processPath;
                    emit processChanged(processPath);

                }

                CloseHandle(Handle);
            }
        }
    });

}

WindowWatcher::~WindowWatcher()
{
    exit = true;
}

void WindowWatcher::startPolling()
{
    started = true;
}

void WindowWatcher::stopPolling()
{
    started = false;
}
