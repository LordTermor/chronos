#include "ProcessTimingModel.h"
#include <QFile>
#include <QTextStream>
#include <QFileInfo>
ProcessTimingModel::ProcessTimingModel(QObject *parent) : QAbstractTableModel(parent)
{

}

int ProcessTimingModel::rowCount(const QModelIndex &parent) const
{
    return m_list.length();
}

int ProcessTimingModel::columnCount(const QModelIndex &parent) const
{
    return 3+(m_showTime?1:0);
}

QVariant ProcessTimingModel::data(const QModelIndex &index, int role) const
{
    if (role==Qt::DisplayRole){
        switch (index.column()) {
        case 0:
            return QFileInfo(m_list[index.row()].fileName).fileName();
        case 1:
            return m_list[index.row()].startTime.toString("yyyy MM dd HH:mm:ss");
        case 2:
            return m_list[index.row()].stopTime.toString("yyyy MM dd HH:mm:ss");
        case 3:
            return m_list[index.row()].time().toString("HH:mm:ss");
        }
    }
    return QVariant::Invalid;
}

QVariant ProcessTimingModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation==Qt::Horizontal){
        if(role==Qt::DisplayRole){
            switch (section) {
            case 0:
                return "Имя процесса";
            case 1:
                return "Начало работы";
            case 2:
                return "Окончание работы";
            case 3:
                return "Время";
            }
        }
    }
    return QVariant::Invalid;
}

QList<ProcessTiming> ProcessTimingModel::list() const
{
    return m_list;
}

void ProcessTimingModel::replaceItem(int index, ProcessTiming &timing)
{
    beginResetModel();
    m_list[index] = timing;
    endResetModel();
}

void ProcessTimingModel::addItem(const ProcessTiming &timing)
{
    beginResetModel();
    m_list.append(timing);
    endResetModel();
}

void ProcessTimingModel::saveJournal(QList<ProcessTiming> timings, QString journalFileName)
{
    QFile file(journalFileName);
    if (file.open(QFile::WriteOnly|QFile::Truncate))
    {
        QTextStream stream(&file);
        for(auto el : timings){
            stream << el.fileName << "\t" << el.startTime.toString(Qt::DateFormat::ISODate) << "\t" << el.stopTime.toString(Qt::DateFormat::ISODate) << "\n";
        }
        file.close();
    }
}

QList<ProcessTiming> ProcessTimingModel::loadJournal(QString journalFileName)
{
    QList<ProcessTiming> result;
    QFile file(journalFileName);

    if (file.open(QFile::ReadOnly))
    {
        QTextStream stream(&file);
        while(!stream.atEnd()){
            QString line = stream.readLine();

            auto list = line.split("\t");
            if(list.length()==3){
                result.append({list[0],
                               QDateTime::fromString(list[1],Qt::DateFormat::ISODate),
                               QDateTime::fromString(list[2],Qt::DateFormat::ISODate) });
            }
        }

        file.close();
    }
    return result;
}

bool ProcessTimingModel::showTime() const
{
    return m_showTime;
}

void ProcessTimingModel::setShowTime(bool showTime)
{
    beginResetModel();
    m_showTime = showTime;
    endResetModel();
}


void ProcessTimingModel::setList(const QList<ProcessTiming> &list)
{
    beginResetModel();
    m_list = list;
    endResetModel();
}
