#include "JournalViewer.h"
#include "ui_JournalViewer.h"

JournalViewer::JournalViewer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::JournalViewer)
{
    ui->setupUi(this);
    ui->tableView->setModel(&model);


}

JournalViewer::~JournalViewer()
{
    delete ui;
}

QString JournalViewer::getFileName() const
{
    return fileName;
}

bool JournalViewer::load(const QString &value)
{
    list = ProcessTimingModel::loadJournal(value);
    if(list.isEmpty()){
        return false;
    }

    model.setList(list);

    fileName = value;
    QDateTime min = list[0].startTime;
    QDateTime max = list[0].stopTime;
    std::for_each(list.begin(),list.end(),[&](const ProcessTiming& a){
        min = std::min(a.startTime,min);
        max = std::max(a.stopTime,max);
    });

    ui->dateTimeEdit->setDateTime(min);
    ui->dateTimeEdit_2->setDateTime(max);

    return true;

}


void JournalViewer::on_dateTimeEdit_2_dateTimeChanged(const QDateTime &dateTime)
{
    filter();
}

void JournalViewer::on_dateTimeEdit_dateTimeChanged(const QDateTime &dateTime)
{
    filter();
}

void JournalViewer::on_lineEdit_textEdited(const QString &arg1)
{
    filter();
}

void JournalViewer::filter()
{
    std::function<void(const ProcessTiming&)> filterFunc;
    QList<ProcessTiming> filtered;

    if(ui->lineEdit->text().isEmpty()){
        filterFunc = [&](const ProcessTiming& process){
            if(process.stopTime<=ui->dateTimeEdit_2->dateTime() &&
                    process.startTime>=ui->dateTimeEdit->dateTime()){
                filtered.append(process);
            }
        };
    } else{
        filterFunc = [&](const ProcessTiming& process){
            if(process.stopTime<=ui->dateTimeEdit_2->dateTime() &&
                    process.startTime>=ui->dateTimeEdit->dateTime() &&
                    process.fileName.toLower().contains(ui->lineEdit->text().toLower())){
                filtered.append(process);
            }
        };
    }
    std::for_each(list.begin(),list.end(),filterFunc);
    model.setList(filtered);

   QDateTime result = std::accumulate(filtered.begin(),filtered.end(),QDateTime::fromTime_t(0),[](QDateTime time,const ProcessTiming& timing){
           return  QDateTime::fromTime_t(time.toTime_t()+timing.time().toTime_t());
    });
    ui->summaryTimeLineEdit->setText(result.toTimeSpec(Qt::OffsetFromUTC).toString("HH:mm:ss"));
}
