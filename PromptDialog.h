#pragma once
#include <QDialog>

namespace Ui {
class PasswordPromptDialog;
}

class PromptDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PromptDialog(QWidget *parent = nullptr);
    ~PromptDialog();

    static bool getIsAuthorized(const QByteArray& hash);

    static bool activate();

private:
    QString password;
    Ui::PasswordPromptDialog *ui;
};
