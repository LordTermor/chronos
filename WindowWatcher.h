#pragma once
#include <windows.h>
#include <QObject>
class WindowWatcher: public QObject
{
    Q_OBJECT
public:
    WindowWatcher(QObject* parent = nullptr);
    ~WindowWatcher();



public slots:
   void startPolling();
   void stopPolling();

signals:
    void processChanged(QString processFileName);
private:
    QString lastProcessPath;
    bool started = false;
    bool exit = false;
};
