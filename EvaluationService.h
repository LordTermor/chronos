#pragma once
#include <QObject>

class EvaluationService : public QObject
{
    Q_OBJECT
public:
    explicit EvaluationService(QObject *parent = nullptr);

    void setupEvaluation();
public slots:
    bool checkEvaluation();
    uint8_t daysLeft();
signals:
    void evaluationPeriodIsOver();

private:
    QDateTime decypherDate();
};
