#pragma once
#include <QString>
#include <QDateTime>

struct ProcessTiming{

    QString fileName;

    QDateTime startTime;
    QDateTime stopTime;

    QDateTime time() const{
        auto seconds = startTime.secsTo(stopTime);
        auto date = QDateTime::fromTime_t(seconds);
        return date.toTimeSpec(Qt::OffsetFromUTC);
    }
};
