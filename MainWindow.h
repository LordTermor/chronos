#pragma once
#include <QMainWindow>
#include <ProcessTimingModel.h>
#include <ProcessTimer.h>
#include <JournalViewer.h>
#include <WhitelistDialog.h>
#include <PreferencesDialog.h>
#include <EvaluationService.h>
#include <QSystemTrayIcon>
#include <WindowWatcher.h>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    bool m_needsRestart = false;

    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void start(){
        watcher.startPolling();
        setStarted(true);
    }

private slots:
    void on_pushButton_clicked();

    void on_action_about_triggered();

    void on_action_close_triggered();

    void on_action_journal_open_triggered();

    void on_action_whitelist_settings_triggered();

    void on_action_preferences_triggered();

    void on_pushButton_2_clicked();

    void on_action_whitelist_export_triggered();

    void on_action_whitelist_import_triggered();

    void on_action_journal_export_triggered();

    void on_action_activate_triggered();

private:
    void setStarted(bool started);
    bool m_started;
    ProcessTimer timer;
    ProcessTimingModel model;
    EvaluationService evaluationService;

    JournalViewer journalDialog;
    WhitelistDialog whiteListDialog;
    PreferencesDialog preferencesDialog;

    WindowWatcher watcher;

    QSystemTrayIcon icon;

    Ui::MainWindow *ui;

    // QWidget interface
protected:
    void closeEvent(QCloseEvent *event);
};
